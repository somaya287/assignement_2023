package ma.octo.assignement.controller;

import lombok.AllArgsConstructor;
import ma.octo.assignement.models.Utilisateur;
import ma.octo.assignement.service.UtilisateurService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/users")
public class UtilisateurController {

    private UtilisateurService utilisateurService;
    private static final Logger log = LoggerFactory.getLogger(CompteController.class);

    @GetMapping()
    public ResponseEntity<List<Utilisateur>> loadAllUtilisateur() {
        log.info("Lister les utilisateurs");
        return new ResponseEntity<>(
                utilisateurService.getUtilisateurs(),
                HttpStatus.OK
        );
    }

}
