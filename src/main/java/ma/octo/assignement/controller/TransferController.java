package ma.octo.assignement.controller;

import lombok.AllArgsConstructor;
import ma.octo.assignement.models.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.service.TransferService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//The logic should be defined in services, controllers only get requests and call service's functions

@RestController //we specify the path in the RequestMapping anotation
@RequestMapping("/transfers")
@AllArgsConstructor // No need to to use autowired
class TransferController {
    private static final Logger log = LoggerFactory.getLogger(TransferController.class);
    private TransferService transferService;

    @GetMapping("")
    // Response entity contain the response and the HTTP status
    public ResponseEntity<List<Transfer>> loadAllTransfer() {
        log.info("Lister les Transferts");
        return new ResponseEntity<>(
            transferService.getTransfers(),
            HttpStatus.OK
        );
    }

    @PostMapping("")
    public ResponseEntity<Void> createTransfer(@RequestBody TransferDto transferDto) {
        log.info("Exécuter un Transfert");
        Transfer transfer = transferService.createTransfer(transferDto);
        return new ResponseEntity<>(
                HttpStatus.CREATED
        );
    }
}
