package ma.octo.assignement.controller;

import lombok.AllArgsConstructor;
import ma.octo.assignement.models.AuditDeposit;
import ma.octo.assignement.models.AuditTransfer;
import ma.octo.assignement.service.AuditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/audit")
@AllArgsConstructor
public class AuditController {
    private static final Logger log = LoggerFactory.getLogger(AuditController.class);
    private AuditService auditService;

    @GetMapping("/transfers")
    public ResponseEntity<List<AuditTransfer>> loadAllAuditTransfer() {
        log.info("Afficher l'audit des transfers");
        return new ResponseEntity<>(
                auditService.getTransferAudits(),
                HttpStatus.OK
        );
    }

    @GetMapping("/deposits")
    public ResponseEntity<List<AuditDeposit>> loadAllAuditDeposit() {
        log.info("Afficher l'audit des dépots");
        return new ResponseEntity<>(
                auditService.getDepositAudits(),
                HttpStatus.OK
        );
    }

}
