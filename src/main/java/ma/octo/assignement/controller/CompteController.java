package ma.octo.assignement.controller;

import lombok.AllArgsConstructor;
import ma.octo.assignement.models.Compte;
import ma.octo.assignement.service.CompteService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/accounts")
public class CompteController {

    private CompteService compteService;
    private static final Logger log = LoggerFactory.getLogger(CompteController.class);

    @GetMapping("")
    public ResponseEntity<List<Compte>> loadAllCompte() {
        log.info("Lister les comptes");
        return new ResponseEntity<>(
                compteService.getComptes(),
                HttpStatus.OK
        );
    }
}
