package ma.octo.assignement.controller;

import lombok.AllArgsConstructor;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.models.Deposit;
import ma.octo.assignement.service.DepositService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/deposits")
@AllArgsConstructor
public class DepositController {
    private static final Logger log = LoggerFactory.getLogger(TransferController.class);
    private DepositService depositService;

    @GetMapping("")
    public ResponseEntity<List<Deposit>> loadAllDeposit() {
        log.info("Lister tous les dépots");
        return new ResponseEntity<>(
                depositService.getDeposits(),
                HttpStatus.OK
        );
    }

    @PostMapping("")
    public ResponseEntity<Void> createDeposit(@RequestBody DepositDto depositDto) {
        log.info("Exécuter un dépot d'argents");
        Deposit deposit = depositService.createDeposit(depositDto);
        return new ResponseEntity<>(
                HttpStatus.CREATED
        );
    }
}
