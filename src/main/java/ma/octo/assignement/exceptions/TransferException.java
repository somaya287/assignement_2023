package ma.octo.assignement.exceptions;

public class TransferException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public TransferException() {}

  public TransferException(String message) {
    super(message);
  }
}
