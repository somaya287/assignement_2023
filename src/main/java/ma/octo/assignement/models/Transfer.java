package ma.octo.assignement.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "TRAN")
public class Transfer extends Operation{
  @ManyToOne
  private Compte compteEmetteur;
}
