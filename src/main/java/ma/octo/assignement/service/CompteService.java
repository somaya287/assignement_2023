package ma.octo.assignement.service;

import lombok.AllArgsConstructor;
import ma.octo.assignement.models.Compte;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import java.util.List;

@Service
@AllArgsConstructor
public class CompteService {
    private CompteRepository compteRepository;

    public List<Compte> getComptes() {
        List<Compte> all = compteRepository.findAll();
        return CollectionUtils.isEmpty(all) ? null : all;
    }
    // those methods are used by the transferService
    public Compte getCompteByNb(String nrCompte) {
        return compteRepository.findByNrCompte(nrCompte);
    }

    public Compte updateComptes(Compte compte) {
        return compteRepository.save(compte);
    }
}
