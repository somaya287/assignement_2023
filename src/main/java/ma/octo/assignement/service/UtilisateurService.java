package ma.octo.assignement.service;

import lombok.AllArgsConstructor;
import ma.octo.assignement.models.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import java.util.List;

@Service
@AllArgsConstructor
public class UtilisateurService {
    private UtilisateurRepository utilisateurRepository;

    public List<Utilisateur> getUtilisateurs() {
        List<Utilisateur> all = utilisateurRepository.findAll();
        return CollectionUtils.isEmpty(all) ? null : all;
    }
}
