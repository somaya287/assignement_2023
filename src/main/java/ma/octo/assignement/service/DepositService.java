package ma.octo.assignement.service;

import lombok.AllArgsConstructor;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.DepositException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.models.Compte;
import ma.octo.assignement.models.Deposit;
import ma.octo.assignement.repository.DepositRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

@Service
@AllArgsConstructor
public class DepositService {
    public static final int MONTANT_MAXIMAL = 10000;
    private DepositRepository depositRepository;
    private CompteService compteService;
    private DepositMapper depositMapper;
    private AuditService auditService;

    public List<Deposit> getDeposits() {
        List<Deposit> all = depositRepository.findAll();
        return CollectionUtils.isEmpty(all) ? null : all;
    }

    public Deposit createDeposit(DepositDto depositDto)
            throws CompteNonExistantException, DepositException {

        Compte compteBeneficiaire = compteService.getCompteByNb(depositDto.getNrCompteBeneficiaire());

        if (compteBeneficiaire == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (depositDto.getMontant() == null || depositDto.getMontant().equals(BigDecimal.ZERO)) {
            System.out.println("Montant vide");
            throw new DepositException("Montant vide");
        }
        else if (depositDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de dépot dépassé");
            throw new DepositException("Montant maximal de dépot dépassé");
        }
        if (depositDto.getMotif().length() <= 0) {
            System.out.println("Motif vide");
            throw new DepositException("Motif vide");
        }

        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(depositDto.getMontant()));
        compteService.updateComptes(compteBeneficiaire);

        Deposit deposit = depositMapper.depositDtoToDeposit(depositDto);
        deposit.setCompteBeneficiaire(compteBeneficiaire);
        deposit = depositRepository.save(deposit);

        auditService.auditDeposit("Depot de " + depositDto.getMontant().toString() + " dans le compte " +
                depositDto.getNrCompteBeneficiaire() + " D'après Mr/Mme " + depositDto.getNameDepositor());
        return deposit;
    }
}
