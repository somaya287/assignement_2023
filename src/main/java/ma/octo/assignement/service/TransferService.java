package ma.octo.assignement.service;

import lombok.AllArgsConstructor;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransferException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.models.Compte;
import ma.octo.assignement.models.Transfer;
import ma.octo.assignement.repository.TransferRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

@Service
@AllArgsConstructor
public class TransferService {
    public static final int MONTANT_MAXIMAL = 10000;
    private TransferRepository transferRepository;
    private AuditService auditService;
    private CompteService compteService;
    private TransferMapper transferMapper;

    public List<Transfer> getTransfers() {
        List<Transfer> all = transferRepository.findAll();
        // if the collection is empty we should return null, else we should return all
        return CollectionUtils.isEmpty(all) ? null : all;
    }

    public Transfer createTransfer(TransferDto transferDto)
            throws CompteNonExistantException, TransferException, SoldeDisponibleInsuffisantException {

        Compte compteEmetteur = compteService.getCompteByNb(transferDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteService.getCompteByNb(transferDto.getNrCompteBeneficiaire());

        if (compteEmetteur == null || compteBeneficiaire == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }
        // the amount may be 0.xx and it doesn't mean that the amount is empty
        if (transferDto.getMontant() == null || transferDto.getMontant().equals(BigDecimal.ZERO)) {
            System.out.println("Montant vide");
            throw new TransferException("Montant vide");
        }
        else if (transferDto.getMontant().intValue() < 10) {
            System.out.println("Montant minimal de transfer non atteint");
            throw new TransferException("Montant minimal de transfer non atteint");
        }
        else if (transferDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de transfer dépassé");
            throw new TransferException("Montant maximal de transfer dépassé");
        }
        if (transferDto.getMotif().length() <= 0) {
            System.out.println("Motif vide");
            throw new TransferException("Motif vide");
        }
        if (compteEmetteur.getSolde().subtract(transferDto.getMontant()).compareTo(BigDecimal.ZERO) < 0) {
            System.out.println("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }

        // we can't work directly with the compteRepository, and instead the communication is done through services
        // We define the methods in compteService and call them here !
        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(transferDto.getMontant()));
        compteService.updateComptes(compteEmetteur);
        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(transferDto.getMontant()));
        compteService.updateComptes(compteBeneficiaire);

        Transfer transfer = transferMapper.transferDtoToTransfer(transferDto);
        transfer.setCompteBeneficiaire(compteBeneficiaire);
        transfer.setCompteEmetteur(compteEmetteur);
        transfer = transferRepository.save(transfer);

        auditService.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant().toString());
        return transfer;
    }
}
