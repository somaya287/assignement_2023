package ma.octo.assignement.service;

import lombok.AllArgsConstructor;
import ma.octo.assignement.models.Audit;
import ma.octo.assignement.models.AuditDeposit;
import ma.octo.assignement.models.AuditTransfer;
import ma.octo.assignement.enums.EventType;
import ma.octo.assignement.repository.AuditDepositRepository;
import ma.octo.assignement.repository.AuditTransferRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@AllArgsConstructor
public class AuditService {

    private static final Logger log = LoggerFactory.getLogger(AuditService.class);
    private AuditTransferRepository auditTransferRepository;
    private AuditDepositRepository auditDepositRepository;

    public void auditTransfer(String message) {
        log.info("Audit de l'événement {}", EventType.TRANSFER);
        AuditTransfer auditTransfer = new AuditTransfer();
        auditTransfer.setEventType(EventType.TRANSFER);
        auditTransfer.setMessage(message);
        auditTransferRepository.save(auditTransfer);
    }

    public void auditDeposit(String message) {
        log.info("Audit de l'événement {}", EventType.DEPOSIT);
        AuditDeposit auditDeposit = new AuditDeposit();
        auditDeposit.setEventType(EventType.DEPOSIT);
        auditDeposit.setMessage(message);
        auditDepositRepository.save(auditDeposit);
    }

    public List<AuditTransfer> getTransferAudits() {
        List<AuditTransfer> all = auditTransferRepository.findAll();
        return CollectionUtils.isEmpty(all) ? null : all;
    }

    public List<AuditDeposit> getDepositAudits() {
        List<AuditDeposit> all = auditDepositRepository.findAll();
        return CollectionUtils.isEmpty(all) ? null : all;
    }
}
