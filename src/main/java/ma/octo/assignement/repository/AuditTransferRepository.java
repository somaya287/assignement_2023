package ma.octo.assignement.repository;

import ma.octo.assignement.models.AuditTransfer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuditTransferRepository extends JpaRepository<AuditTransfer, Long> {
}
