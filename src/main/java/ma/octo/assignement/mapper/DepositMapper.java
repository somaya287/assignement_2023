package ma.octo.assignement.mapper;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.models.Deposit;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DepositMapper {
    Deposit depositDtoToDeposit(DepositDto depositDto);
    DepositDto depositToDepositDto(Deposit deposit);
}
