package ma.octo.assignement.mapper;

import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.models.Transfer;
import org.mapstruct.Mapper;

//Mapstruct simplify the implementation of mapping between two beans
@Mapper(componentModel = "spring")
public interface TransferMapper {
    Transfer transferDtoToTransfer(TransferDto transferDto);
    TransferDto transferToTransferDto(Transfer transfer);
}
