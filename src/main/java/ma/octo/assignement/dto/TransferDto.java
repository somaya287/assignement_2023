package ma.octo.assignement.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class TransferDto {
  private String nrCompteEmetteur;
  private String nrCompteBeneficiaire;
  private String motif;
  private BigDecimal montant;
  private Date dateExecution;
}
//I named the attributes exactly like the names in the Transfer model, so that the automapping using lombok mapstruct
     // works correctly for common attributes

//Normally, I think that there's no need to have dateExecution in TransferDto, we can get the actual date when executing the transfer
// But maybe we can have that option of scheduling transfer and deposit, so I'll keep it the way it was
